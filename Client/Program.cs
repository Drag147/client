﻿using Client.Controller;
using ImageMagick;
using System;
using System.Windows.Forms;

namespace Client
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MagickNET.SetGhostscriptDirectory(AppDomain.CurrentDomain.BaseDirectory);
            Application.Run(new WindowController().LoginWindow);
        }
    }
}
