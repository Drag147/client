﻿using Client.Services.Crypto.Kuznechik;
using DrmCommon.Dto.Contents;
using DrmCommon.Request;
using DrmCommon.Request.RequestContent;
using NLog;

namespace Client.Services
{
    class RequestContentService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly KuznechikCryptoService kuznechikCryptoService;

        public RequestContentService(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;
        }

        public Request GetListMyContents(string login)
        {
            RequestGetListContents requestGetListMyContent = new RequestGetListContents();
            requestGetListMyContent.waitResponse = true;
            requestGetListMyContent.typeRequest = TypeRequest.GetListUserContents;
            requestGetListMyContent.IDKey = kuznechikCryptoService.userKey.IDKey;
            requestGetListMyContent.userLogin = kuznechikCryptoService.Encrypt(login, Crypto.CryptoService.TypeKeyUse.SessionKey);
            return requestGetListMyContent;
        }

        public Request GetListContentsAll()
        {
            RequestGetListContents requestGetListMyContent = new RequestGetListContents();
            requestGetListMyContent.waitResponse = true;
            requestGetListMyContent.typeRequest = TypeRequest.GetListContentsAll;
            return requestGetListMyContent;
        }

        public RequestOpenContent requestOpenContent(int idContent, string login)
        {
            RequestOpenContent requestOpenContent = new RequestOpenContent();
            ContentsDto contentsDto = new ContentsDto();
            contentsDto.id = idContent;
            requestOpenContent.contentDto = contentsDto;
            requestOpenContent.typeRequest = TypeRequest.GetOpenContent;
            requestOpenContent.userLogin = kuznechikCryptoService.Encrypt(login, Crypto.CryptoService.TypeKeyUse.SessionKey);
            requestOpenContent.IDKey = kuznechikCryptoService.userKey.IDKey;
            requestOpenContent.waitResponse = true;

            return requestOpenContent;
        }
    }
}
