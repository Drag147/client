﻿using Client.Services.Crypto.Kuznechik;
using NLog;
using System;
using Client.Service.Crypto.UserKey;
using DrmCommon.Request;
using DrmCommon.Response;
using DrmCommon.Dto.Users;
using static Client.Services.Crypto.CryptoService;
using DrmCommon.Helper;

namespace Client.Services
{
    public class AuthorizationService
    {
        private readonly KuznechikCryptoService kuznechikCryptoService;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public AuthorizationService(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;
        }

        public Request LoginOrRegistration(TypeRequest typeRequest,
            string Login,
            string Password,
            string Name = null,
            string Surname = null,
            string Patronymic = null,
            string Mail = null)
        {
            RequestAuthorization request = new RequestAuthorization();
            request.waitResponse = true;
            switch (typeRequest)
            {
                case TypeRequest.RequestAuthorization:
                    request.IDKey = kuznechikCryptoService.userKey.IDKey;
                    request.typeRequest = TypeRequest.RequestAuthorization;
                    request.CurrentUserDtoEncripted = new UserDto(kuznechikCryptoService.Encrypt(Login, TypeKeyUse.SessionKey),
                        kuznechikCryptoService.Encrypt(kuznechikCryptoService.GetHash(Password), TypeKeyUse.SessionKey));
                    break;
                case TypeRequest.Registration:
                    request.IDKey = kuznechikCryptoService.userKey.IDKey;
                    request.typeRequest = TypeRequest.Registration;
                    request.CurrentUserDtoEncripted = new UserDto()
                    {
                        login = kuznechikCryptoService.Encrypt(Login, TypeKeyUse.SessionKey),
                        passwordHash = kuznechikCryptoService.Encrypt(kuznechikCryptoService.GetHash(Password), TypeKeyUse.SessionKey),
                        name = kuznechikCryptoService.Encrypt(Name, TypeKeyUse.SessionKey),
                        surname = kuznechikCryptoService.Encrypt(Surname, TypeKeyUse.SessionKey),
                        patronymic = kuznechikCryptoService.Encrypt(Patronymic, TypeKeyUse.SessionKey),
                        mail = kuznechikCryptoService.Encrypt(Mail, TypeKeyUse.SessionKey)
                    };
                    break;
                default:
                    break;
            }
            return request;
        }

        public RequestGenerateSessionKeys RequestForGenerateSessionKeyStep1()
        {
            RequestGenerateSessionKeys request = new RequestGenerateSessionKeys();
            request.typeRequest = TypeRequest.GenerateSessionKeyStep1;
            request.waitResponse = true;
            return request;
        }

        public RequestGenerateSessionKeys RequestForGenerateSessionKeyStep2(
            ResponseGenerateSessionKeys responseGenerateSessionKeys1,
            Random rand,
            ref StructureUserKey UserKey)
        {
            BigInteger Ys = new BigInteger();
            Ys.genRandomBits(256, rand);
            while (Ys >= responseGenerateSessionKeys1.sessionKeyGenerateDto.N)
            {
                Ys.genRandomBits(256, rand);
            }
            BigInteger Y = responseGenerateSessionKeys1.sessionKeyGenerateDto.G
                .modPow(Ys, responseGenerateSessionKeys1.sessionKeyGenerateDto.N);

            UserKey.IDKey = responseGenerateSessionKeys1.sessionKeyGenerateDto.IDKey;
            UserKey.SessionKey = responseGenerateSessionKeys1.sessionKeyGenerateDto.X.modPow(
                Ys, responseGenerateSessionKeys1.sessionKeyGenerateDto.N);
            
            RequestGenerateSessionKeys requestStep2Success = new RequestGenerateSessionKeys();
            requestStep2Success.IDKey = responseGenerateSessionKeys1.sessionKeyGenerateDto.IDKey;
            requestStep2Success.sessionKeyGenerateDto = responseGenerateSessionKeys1.sessionKeyGenerateDto;
            requestStep2Success.sessionKeyGenerateDto.Y = Y;
            requestStep2Success.typeRequest = TypeRequest.GenerateSessionKeyStep2;
            requestStep2Success.waitResponse = true;
            return requestStep2Success;
        }
    }
}
