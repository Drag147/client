﻿using Client.Service.Crypto.UserKey;
using DrmCommon.Helper;
using DrmCommon.Request;
using DrmCommon.Response;
using System;

namespace Client.Services.Crypto
{
    public class UpdateUserKey
    {
        public RequestGenerateSessionKeys RequestForGenerateSessionKeyStep1(string login,
            Kuznechik.KuznechikCryptoService kuznechikCryptoService)
        {
            RequestGenerateSessionKeys request = new RequestGenerateSessionKeys();
            request.typeRequest = TypeRequest.GenerateSessionKeyStep1;
            request.waitResponse = true;
            request.userLogin = kuznechikCryptoService.Encrypt(login, CryptoService.TypeKeyUse.SessionKey);
            request.IDKey = kuznechikCryptoService.userKey.IDKey;
            return request;
        }

        public RequestGenerateSessionKeys RequestForGenerateSessionKeyStep2(
            ResponseGenerateSessionKeys responseGenerateSessionKeys1,
            Random rand,
            ref StructureUserKey UserKey,
            Kuznechik.KuznechikCryptoService kuznechikCryptoService,
            string login)
        {
            BigInteger Ys = new BigInteger();
            Ys.genRandomBits(256, rand);
            while (Ys >= responseGenerateSessionKeys1.sessionKeyGenerateDto.N)
            {
                Ys.genRandomBits(256, rand);
            }
            BigInteger Y = responseGenerateSessionKeys1.sessionKeyGenerateDto.G
                .modPow(Ys, responseGenerateSessionKeys1.sessionKeyGenerateDto.N);

            UserKey.IDKey = responseGenerateSessionKeys1.sessionKeyGenerateDto.IDKey;
            UserKey.SessionKey = responseGenerateSessionKeys1.sessionKeyGenerateDto.X.modPow(
                Ys, responseGenerateSessionKeys1.sessionKeyGenerateDto.N);

            RequestGenerateSessionKeys requestStep2Success = new RequestGenerateSessionKeys();
            requestStep2Success.IDKey = responseGenerateSessionKeys1.sessionKeyGenerateDto.IDKey;
            requestStep2Success.userLogin = kuznechikCryptoService.Encrypt(login, CryptoService.TypeKeyUse.SessionKey);
            requestStep2Success.sessionKeyGenerateDto = responseGenerateSessionKeys1.sessionKeyGenerateDto;
            requestStep2Success.sessionKeyGenerateDto.Y = Y;
            requestStep2Success.typeRequest = TypeRequest.GenerateSessionKeyStep2;
            requestStep2Success.waitResponse = true;
            return requestStep2Success;
        }
    }
}
