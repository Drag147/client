﻿using NLog;
using Client.Services.Crypto.Helper;
using System.Text;
using DrmCommon.Helper;

namespace Client.Services.Crypto
{
    public abstract class CryptoService
    {
        public enum TypeKeyUse
        {
            SessionKey,
            ContentKey
        }

        private readonly CStribog cStribog;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public CryptoService()
        {
            this.cStribog = new CStribog(Properties.Settings.Default.lengthHash);
        }

        public string GetHash (string dataString)
        {
            byte[] data = Encoding.GetEncoding(1251).GetBytes(dataString);

            byte[] hashData = cStribog.GetHash(data);

            return new BigInteger(hashData).ToHexString();
        }

        public abstract string Encrypt(string data, TypeKeyUse typeKeyUse);
        public abstract string Decrypt(string data, TypeKeyUse typeKeyUse);
    }
}
