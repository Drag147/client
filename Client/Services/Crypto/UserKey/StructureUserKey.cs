﻿using DrmCommon.Helper;

namespace Client.Service.Crypto.UserKey
{
    public class StructureUserKey
    {
        public int IDKey { get; set; }
        public BigInteger KeyForContent { get; set; }
        public BigInteger SessionKey { get; set; }

        public StructureUserKey()
        {
            this.IDKey = -1;
            KeyForContent = new BigInteger();
            SessionKey = new BigInteger();
        }
    }
}
