﻿using Client.Service.Crypto.UserKey;
using NLog;
using System;
using System.Numerics;
using System.Text;

namespace Client.Services.Crypto.Kuznechik
{
    public class KuznechikCryptoService : CryptoService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private const string SYMBOL_FOR_ADD_END_BLOCK = "\0";

        private readonly KuznechikOnVectors kuznechikOnVectors = new KuznechikOnVectors();
        public StructureUserKey userKey = new StructureUserKey();

        public KuznechikCryptoService()
        {

        }

        public void RemoveAllKey()
        {
            this.userKey = new StructureUserKey();
        }

        public override string Encrypt(string data, TypeKeyUse typeKeyUse)
        {
            Vector<byte>[] roundKeys = new Vector<byte>[10];

            switch (typeKeyUse)
            {
                case TypeKeyUse.SessionKey:
                    kuznechikOnVectors.generateEncryptionRoundKeys(userKey.SessionKey.getBytes(), ref roundKeys);
                    break;
                case TypeKeyUse.ContentKey:
                    kuznechikOnVectors.generateEncryptionRoundKeys(userKey.SessionKey.getBytes(), ref roundKeys);
                    break;
                default:
                    kuznechikOnVectors.generateEncryptionRoundKeys(userKey.SessionKey.getBytes(), ref roundKeys);
                    break;
            }

            byte[] textBytes = Encoding.GetEncoding(1251).GetBytes(data);

            if (textBytes.Length % 16 != 0)
            {
                textBytes.CopyTo(textBytes = new byte[textBytes.Length + 16 - (textBytes.Length % 16)], 0);
            }

            byte[] block = new byte[16];
            Vector<byte> tmp;

            for (int i = 0; i < textBytes.Length / 16; i++)
            {
                tmp = new Vector<byte>(textBytes, i * 16);
                kuznechikOnVectors.encrypt(ref tmp, roundKeys);
                tmp.CopyTo(block, 0);
                Buffer.BlockCopy(block, 0, textBytes, i * 16, 16);
            }

            return Encoding.GetEncoding(1251).GetString(textBytes);
        }

        public override string Decrypt(string data, TypeKeyUse typeKeyUse)
        {
            Vector<byte>[] roundKeys = new Vector<byte>[10];

            switch (typeKeyUse)
            {
                case TypeKeyUse.SessionKey:
                    kuznechikOnVectors.generateDencryptionRoundKeys(userKey.SessionKey.getBytes(), ref roundKeys);
                    break;
                case TypeKeyUse.ContentKey:
                    kuznechikOnVectors.generateDencryptionRoundKeys(userKey.SessionKey.getBytes(), ref roundKeys);
                    break;
                default:
                    kuznechikOnVectors.generateDencryptionRoundKeys(userKey.SessionKey.getBytes(), ref roundKeys);
                    break;
            }

            byte[] textBytes = Encoding.GetEncoding(1251).GetBytes(data);

            if (textBytes.Length % 16 != 0)
            {
                textBytes.CopyTo(textBytes = new byte[textBytes.Length + 16 - (textBytes.Length % 16)], 0);
            }

            byte[] block = new byte[16];
            Vector<byte> tmp;

            for (int i = 0; i < textBytes.Length / 16; i++)
            {
                tmp = new Vector<byte>(textBytes, i * 16);
                kuznechikOnVectors.decrypt(ref tmp, roundKeys);
                tmp.CopyTo(block, 0);
                Buffer.BlockCopy(block, 0, textBytes, i * 16, 16);
            }

            return Encoding.GetEncoding(1251).GetString(textBytes).Replace(SYMBOL_FOR_ADD_END_BLOCK, "");
        }

        public byte[] DecryptContent(byte[] textBytes, int sizeContent)
        {
            try
            {
                Vector<byte>[] roundKeys = new Vector<byte>[10];

                kuznechikOnVectors.generateDencryptionRoundKeys(userKey.KeyForContent.getBytes(), ref roundKeys);

                byte[] block = new byte[16];
                Vector<byte> tmp;

                byte[] resultData = new byte[sizeContent];

                int nowSize = 0;
                for (int i = 0; i < textBytes.Length / 16; i++)
                {
                    tmp = new Vector<byte>(textBytes, i * 16);
                    kuznechikOnVectors.decrypt(ref tmp, roundKeys);
                    tmp.CopyTo(block, 0);
                    if(nowSize + 16 > sizeContent)
                    {
                        Buffer.BlockCopy(block, 0, resultData, i * 16, sizeContent - nowSize);
                    }
                    else
                    {
                        Buffer.BlockCopy(block, 0, resultData, i * 16, 16);
                    }
                    nowSize += 16;
                }

                return resultData;
            }
            catch { }
            return textBytes;
        }

        private string[] StringSplitter128(string data)
        {
            //8 бит на символ Windows-1251 128-bit блок
            int chunkSize = 16;
            int inputStringSize = data.Length;
            double doubleVarChunkCount = (double)inputStringSize / (double)chunkSize;
            int chunkCount = (int)Math.Ceiling(doubleVarChunkCount);
            if (inputStringSize < chunkCount * chunkSize)
            {
                for (int j = inputStringSize; j < chunkCount * chunkSize; j++)
                {
                    data += SYMBOL_FOR_ADD_END_BLOCK;
                }
            }

            string[] block = new string[chunkCount];
            for (int i = 0; i < chunkCount; i++)
            {
                block[i] = data.Substring(i * chunkSize, chunkSize);
            }

            return block;
        }
    }
}
