﻿using Client.Services.Crypto.Kuznechik;
using DrmCommon.Dto.Contents;
using DrmCommon.Request;
using DrmCommon.Request.RequestContent;
using NLog;

namespace Client.Services
{
    class BuyDeleteContentService
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly KuznechikCryptoService kuznechikCryptoService;

        public BuyDeleteContentService(KuznechikCryptoService kuznechikCryptoService)
        {
            this.kuznechikCryptoService = kuznechikCryptoService;
        }

        public RequestBuyDeleteContent requestBuyContent(int idContent, string login)
        {
            RequestBuyDeleteContent requestBuy = new RequestBuyDeleteContent();
            ContentsDto contentsDto = new ContentsDto();
            contentsDto.id = idContent;
            requestBuy.contentDto = contentsDto;
            requestBuy.typeRequest = TypeRequest.BuyContent;
            requestBuy.IDKey = kuznechikCryptoService.userKey.IDKey;
            requestBuy.userLogin = kuznechikCryptoService.Encrypt(login, Crypto.CryptoService.TypeKeyUse.SessionKey);
            requestBuy.waitResponse = true;

            return requestBuy;
        }

        public RequestBuyDeleteContent requestDeleteContent(int idContent, string login)
        {
            RequestBuyDeleteContent requestDelete = new RequestBuyDeleteContent();
            ContentsDto contentsDto = new ContentsDto();
            contentsDto.id = idContent;
            requestDelete.contentDto = contentsDto;
            requestDelete.typeRequest = TypeRequest.DeleteContent;
            requestDelete.IDKey = kuznechikCryptoService.userKey.IDKey;
            requestDelete.userLogin = kuznechikCryptoService.Encrypt(login, Crypto.CryptoService.TypeKeyUse.SessionKey);
            requestDelete.waitResponse = true;

            return requestDelete;
        }
    }
}
