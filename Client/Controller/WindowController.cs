﻿using System;
using System.Windows.Forms;
using NLog;
using Client.Forms;
using Client.Services;
using Client.ConnectionManager;
using System.Collections.Generic;
using ImageMagick;
using Client.Services.Crypto.Kuznechik;
using DrmCommon.Request;
using DrmCommon.Response;
using DrmCommon.Result;
using DrmCommon.Dto.Contents;
using DrmCommon.Helper;
using Client.Services.Crypto;
using System.Threading;
using System.Threading.Tasks;

namespace Client.Controller
{
    enum StateForms
    {
        Login,
        Register,
        MainWindow
    }

    public class WindowController
    {
        private readonly NetworkConnectionManager networkConnectionManager = NetworkConnectionManager.GetNetworkConnectionManager();
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private string login;

        private Random rand = new Random();
        public readonly LoginWindow LoginWindow;
        public MainWindow mainWindow;
        private RegistrationWindow registrationWindow;
        private bool flagCreateNewFormRegister = true;
        private bool flagCreateNewMainForm = false;
        private bool LogoutState = false;

        private StateForms StateForms = StateForms.Login;

        private readonly AuthorizationService authorizationService;
        private readonly BuyDeleteContentService buyContentService;
        private readonly RequestContentService requestContentService;
        private readonly UpdateUserKey updateUserKeyService = new UpdateUserKey();
        private readonly KuznechikCryptoService kuznechikCryptoService = new KuznechikCryptoService();

        public WindowController()
        {
            this.LoginWindow = new LoginWindow(this);
            this.mainWindow = new MainWindow(this);
            this.registrationWindow = new RegistrationWindow(this);
            this.LoginWindow.FormClosed += this.CloseAllForms;
            this.mainWindow.FormClosed += this.CloseAllForms;

            this.registrationWindow.FormClosed += this.CloseRegisterForm;

            this.LoginWindow.FormClosing += this.BlockCreateNewForms;
            this.mainWindow.FormClosing += this.BlockCreateNewForms;

            this.mainWindow.FormClosed += this.LogoutOpenLoginForm;

            authorizationService = new AuthorizationService(kuznechikCryptoService);
            buyContentService = new BuyDeleteContentService(kuznechikCryptoService);
            requestContentService = new RequestContentService(kuznechikCryptoService);
        }

        /* Авторизация */
        public void ShowRegistrationForm()
        {
            if (this.StateForms == StateForms.Login)
            {
                this.LoginWindow.Hide();
                this.registrationWindow.Show(this.LoginWindow);

                this.StateForms = StateForms.Register;
            }
        }

        private bool GenerateSessionKey()
        {
            if (kuznechikCryptoService.userKey == null || kuznechikCryptoService.userKey.IDKey == -1)
            {
                //Первый шаг. Получаем IDKey, N, G, X.
                RequestGenerateSessionKeys requestGenerateKeyStep1 = this.authorizationService
                    .RequestForGenerateSessionKeyStep1();
                Response responseGenerateKeyStep1 = networkConnectionManager.Send(requestGenerateKeyStep1);
                //Первый шаг конец. Пришло IDKey, N, G, X.

                if (responseGenerateKeyStep1 is ResponseGenerateSessionKeys responseGenerateSessionKeys1)
                {
                    //Второй шаг. Отправляем IDKey, N, G, X, Y.
                    RequestGenerateSessionKeys requestGenerateKeyStep2 = this.authorizationService
                        .RequestForGenerateSessionKeyStep2(responseGenerateSessionKeys1, rand, ref kuznechikCryptoService.userKey);
                    Response responseGenerateKeyStep2 = networkConnectionManager.Send(requestGenerateKeyStep2);
                    //Ждём подтверждения что сервер сгенерировал и положил себе сеансовый ключ.

                    //Если пришёл отказ, то удаляем наши ключи и тормозим все действия.
                    if (!responseGenerateKeyStep2.resultOperation.success)
                    {
                        this.kuznechikCryptoService.RemoveAllKey();
                        this.ShowMessageResultOperation(responseGenerateKeyStep2.resultOperation);
                        return false;
                    }

                    this.ShowMessageResultOperation(responseGenerateKeyStep2.resultOperation);
                }
                else
                {
                    //Нам пришёл не тот ответ который мы ожидали.
                    this.kuznechikCryptoService.RemoveAllKey();
                    ResultOperation resultOperationError = new ResultOperation();
                    resultOperationError.AddNewErrorMessage("Произошла ошибка при генерации ключей. Повторите попытку...");
                    this.ShowMessageResultOperation(resultOperationError);
                    return false;
                }
            }
            return true;
        }

        public bool UpdateSessionKey()
        {
            if (kuznechikCryptoService.userKey != null || kuznechikCryptoService.userKey.IDKey != -1)
            {
                //Первый шаг. Получаем IDKey, N, G, X.
                RequestGenerateSessionKeys requestGenerateKeyStep1 = updateUserKeyService.RequestForGenerateSessionKeyStep1(login,
                    this.kuznechikCryptoService);

                kuznechikCryptoService.RemoveAllKey();

                Response responseGenerateKeyStep1 = networkConnectionManager.Send(requestGenerateKeyStep1);
                //Первый шаг конец. Пришло IDKey, N, G, X.

                if (responseGenerateKeyStep1 is ResponseGenerateSessionKeys responseGenerateSessionKeys1)
                {
                    //Второй шаг. Отправляем IDKey, N, G, X, Y.
                    RequestGenerateSessionKeys requestGenerateKeyStep2 = this.updateUserKeyService
                        .RequestForGenerateSessionKeyStep2(
                        responseGenerateSessionKeys1,
                        rand,
                        ref kuznechikCryptoService.userKey,
                        kuznechikCryptoService,
                        login);
                    Response responseGenerateKeyStep2 = networkConnectionManager.Send(requestGenerateKeyStep2);
                    //Ждём подтверждения что сервер сгенерировал и положил себе сеансовый ключ.

                    //Если пришёл отказ, то удаляем наши ключи и тормозим все действия.
                    if (!responseGenerateKeyStep2.resultOperation.success)
                    {
                        this.kuznechikCryptoService.RemoveAllKey();
                        this.ShowMessageResultOperation(responseGenerateKeyStep2.resultOperation);
                        return false;
                    }

                    this.ShowMessageResultOperation(responseGenerateKeyStep2.resultOperation);
                }
                else
                {
                    //Нам пришёл не тот ответ который мы ожидали.
                    this.kuznechikCryptoService.RemoveAllKey();
                    ResultOperation resultOperationError = new ResultOperation();
                    resultOperationError.AddNewErrorMessage("Произошла ошибка при генерации ключей. Повторите попытку...");
                    this.ShowMessageResultOperation(resultOperationError);
                    return false;
                }
            }
            return true;
        }

        public void Login(TypeRequest typeRequest, string Login, string Password)
        {
            if (this.StateForms == StateForms.Login)
            {
                //генерация сеансовых ключей
                if (this.GenerateSessionKey())
                {
                    //Отправляем запрос на авторизацию шифруя существуюим ключом.
                    Request request = this.authorizationService.LoginOrRegistration(typeRequest, Login, Password);
                    Response response = networkConnectionManager.Send(request);

                    ShowMessageResultOperation(response.resultOperation);

                    if (response.resultOperation.success)
                    {
                        this.LoginWindow.Hide();
                        this.mainWindow.SetLoginLabel(Login);

                        login = Login;

                        this.mainWindow.buttonRefreshTreeContents_Click(null, null);
                        this.mainWindow.Show(this.LoginWindow);
                        this.StateForms = StateForms.MainWindow;
                    }
                    else
                    {
                        kuznechikCryptoService.RemoveAllKey();
                    }
                }
                else
                {
                    kuznechikCryptoService.RemoveAllKey();
                }
            }
        }

        public void Registration(
            TypeRequest typeRequest,
            string Login,
            string Password,
            string Name = null,
            string Surname = null,
            string Patronymic = null,
            string Mail = null)
        {
            if (this.StateForms == StateForms.Register)
            {
                //Реквест на регистрацию. Сначала генерируем ключи сеансовые.
                if (GenerateSessionKey())
                {
                    Request request = this.authorizationService.LoginOrRegistration(
                        typeRequest,
                        Login,
                         Password,
                        Name,
                        Surname,
                        Patronymic,
                        Mail);
                    Response response = networkConnectionManager.Send(request);

                    ShowMessageResultOperation(response.resultOperation);

                    if (response != null && response.resultOperation.success)
                    {
                        this.LoginWindow.SetTextBoxes(Login, Password);
                        this.registrationWindow.Close();
                    }
                    else
                    {
                        if (response != null)
                        {
                            this.registrationWindow.ClearLoginAndPassword();
                        }
                        //не удалось зарегистрироваться удаляем ключи
                        this.kuznechikCryptoService.RemoveAllKey();
                    }
                }
            }
        }

        public void Logout(string login)
        {
            RequestLogout(login);

            kuznechikCryptoService.RemoveAllKey();
            this.flagCreateNewMainForm = true;
            this.LogoutState = true;
            this.mainWindow.Close();
            this.LoginWindow.ClearForm();
            this.LoginWindow.Show();
        }

        private void RequestLogout(string login)
        {
            RequestLogout requestLogout = new RequestLogout();
            requestLogout.IDKey = kuznechikCryptoService.userKey.IDKey;
            requestLogout.typeRequest = TypeRequest.Logout;
            requestLogout.waitResponse = false;
            requestLogout.login = kuznechikCryptoService.Encrypt(login, Services.Crypto.CryptoService.TypeKeyUse.SessionKey);
            networkConnectionManager.Send(requestLogout);
        }

        public void CloseAllForms(object sender, EventArgs e)
        {
            if (!this.LogoutState)
            {
                flagCreateNewFormRegister = false;
                flagCreateNewMainForm = false;
                Application.Exit();
            }
            else
            {
                this.LogoutState = false;
            }
        }

        public void BlockCreateNewForms(object sender, EventArgs e)
        {
            if (!this.flagCreateNewMainForm)
            {
                flagCreateNewFormRegister = false;
                flagCreateNewMainForm = false;
            }
        }

        public void CloseRegisterForm(object sender, EventArgs e)
        {
            this.StateForms = StateForms.Login;
            if (this.flagCreateNewFormRegister)
            {
                this.registrationWindow = new RegistrationWindow(this);
                this.registrationWindow.FormClosed += this.CloseRegisterForm;
                this.LoginWindow.Show();
            }
        }

        public void LogoutOpenLoginForm(object sender, EventArgs e)
        {
            if (this.flagCreateNewMainForm)
            {
                this.StateForms = StateForms.Login;
                this.mainWindow = new MainWindow(this);
                this.mainWindow.FormClosed += this.CloseAllForms;
                this.mainWindow.FormClosing += this.BlockCreateNewForms;
                this.mainWindow.FormClosed += this.LogoutOpenLoginForm;
                this.flagCreateNewMainForm = false;
            }
        }

        /* Работе с Деревом показа контента */
        public async void RefreshContentsTree(TreeView treeViewContents)
        {
            ResultOperation resultOperation = new ResultOperation();
            resultOperation.success = true;
            if (login == null || login == "")
            {
                resultOperation.AddNewErrorMessage("Вы не авторизированы. Перезайдите в приложение.");
                resultOperation.success = false;
            }
            if (kuznechikCryptoService.userKey == null || kuznechikCryptoService.userKey.IDKey == -1)
            {
                resultOperation.AddNewErrorMessage("У вас нет сессионного ключа. Перезайдите в приложение.");
                resultOperation.success = false;
            }
            if (!resultOperation.success)
            {
                ShowMessageResultOperation(resultOperation);
                return;
            }

            //Запрос всех книг.
            Request requestAll = requestContentService.GetListContentsAll();
            Response responseAll = networkConnectionManager.Send(requestAll);
            if (responseAll is ResponseGetListContents responseGetListContents)
            {
                RefreshTree(treeViewContents, responseGetListContents.listContents, responseAll.typeResponse);
            }

            //Запрос своих книг.
            Request requestMyContent = requestContentService.GetListMyContents(login);
            Response responseMyContent = networkConnectionManager.Send(requestMyContent);
            if (responseMyContent is ResponseGetListContents responseGetListMyContent)
            {
                RefreshTree(treeViewContents, responseGetListMyContent.listContents, responseGetListMyContent.typeResponse);
            }

            if (responseMyContent.NeedUpdateKey)
            {
               await Task.Run(new Action(() => UpdateSessionKey()));
            }
        }

        private void RefreshTree(TreeView treeViewContents, List<ContentsDto> listContents, TypeResponse typeResponse)
        {
            TreeNodeCollection treeNodeCollection = treeViewContents.Nodes;
            TreeNode treeRootNode = null;
            if (typeResponse == TypeResponse.GetListContentsAll)
            {
                treeRootNode = treeNodeCollection[treeNodeCollection.IndexOfKey("allContents")];
            }

            if (typeResponse == TypeResponse.GetListUserContents)
            {
                treeRootNode = treeNodeCollection[treeNodeCollection.IndexOfKey("myContents")];
            }
            treeRootNode.Nodes.Clear();

            TreeNode[] treeNodes = new TreeNode[listContents.Count];
            for (int i = 0; i < listContents.Count; i++)
            {
                TreeNode treeNode = new TreeNode(listContents[i].NameContent);
                treeNode.Name = listContents[i].id.ToString();
                treeNodes[i] = treeNode;
            }
            treeRootNode.Nodes.AddRange(treeNodes);
        }

        /* Покупка контента */
        public void BuyContent(int idContent)
        {
            Request requestBuy = buyContentService.requestBuyContent(idContent, login);
            Response responseBuy = networkConnectionManager.Send(requestBuy);
            if (responseBuy != null && responseBuy.typeResponse == TypeResponse.BuyContent)
            {
                ShowMessageResultOperation(responseBuy.resultOperation);
            }

            if (responseBuy.NeedUpdateKey)
            {
                UpdateSessionKey();
            }
        }

        /* Удаление контента */
        public void DeleteContent(int idContent)
        {
            Request requestBuyDelete = buyContentService.requestDeleteContent(idContent, login);
            Response responseBuyDelete = networkConnectionManager.Send(requestBuyDelete);
            if (responseBuyDelete != null && responseBuyDelete.typeResponse == TypeResponse.DeleteContent)
            {
                ShowMessageResultOperation(responseBuyDelete.resultOperation);
            }

            if (responseBuyDelete.NeedUpdateKey)
            {
                UpdateSessionKey();
            }
        }

        /* Получение контента */
        public MagickImageCollection OpenContent(int idContent, out bool NeedUpdate)
        {
            Request requestOpenContent = requestContentService.requestOpenContent(idContent, login);
            Response responseOpenContent = networkConnectionManager.Send(requestOpenContent);
            if (responseOpenContent != null && responseOpenContent is ResponseGetOpenContent responseGetOpenContent)
            {
                if (responseGetOpenContent != null && responseGetOpenContent.typeResponse == TypeResponse.GetOpenContent)
                {
                    ShowMessageResultOperation(responseGetOpenContent.resultOperation);
                }
                if (responseGetOpenContent != null && responseGetOpenContent.resultOperation.success)
                {
                    if (responseGetOpenContent.encriptedKeyForContent != null && responseGetOpenContent.encriptedKeyForContent != "")
                    {
                        //вытаскиваем ключ и дешифруем его
                        string Decryptkey = kuznechikCryptoService.Decrypt(
                            responseGetOpenContent.encriptedKeyForContent, Services.Crypto.CryptoService.TypeKeyUse.SessionKey);
                        try
                        {
                            kuznechikCryptoService.userKey.KeyForContent = new BigInteger(Decryptkey, 16);
                        }
                        catch (Exception)
                        {
                            ResultOperation ErrorResult = new ResultOperation();
                            ErrorResult.success = false;
                            ErrorResult.AddNewErrorMessage("Ошибка. Невозможно расшифровать ключ.");
                            ShowMessageResultOperation(ErrorResult);
                            NeedUpdate = responseGetOpenContent.NeedUpdateKey;
                            return null;
                        }
                    }
                    try
                    {
                        MagickReadSettings magickSetting = new MagickReadSettings();
                        magickSetting.Compression = CompressionMethod.JPEG2000;
                        magickSetting.Density = new Density(150);

                        NeedUpdate = responseGetOpenContent.NeedUpdateKey;

                        return new MagickImageCollection(
                            kuznechikCryptoService.DecryptContent(responseGetOpenContent.content.ContentData, (int)responseGetOpenContent.content.sizeContent),
                            magickSetting);
                    }
                    catch
                    {
                        ResultOperation resultOperation = new ResultOperation();
                        resultOperation.success = false;
                        resultOperation.AddNewErrorMessage("Ошибка расшифровании контента.");
                        ShowMessageResultOperation(resultOperation);
                    }
                }
            }

            try { NeedUpdate = responseOpenContent.NeedUpdateKey; } catch { NeedUpdate = true; }
            
            return null;
        }

        public void ShowMessageResultOperation(ResultOperation resultOperation)
        {
            if (resultOperation == null || resultOperation.InfoMessages == null 
                || resultOperation.ErrorMessages == null)
            {
                return;
            }
            foreach (var item in resultOperation.InfoMessages)
            {
                MessageBox.Show(item, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            foreach (var item in resultOperation.ErrorMessages)
            {
                MessageBox.Show(item, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
