﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using NLog;
using Client.Services.Crypto.Helper;
using System.IO;
using System.IO.Compression;
using DrmCommon.Response;
using DrmCommon.Request;

namespace Client.ConnectionManager
{
    public class NetworkConnectionManager
    {
        private static NetworkConnectionManager networkConnectionManager = null;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private class ConnectionServerInfo
        {
            public Socket connectionToServerSocket;
            public IPEndPoint ipEndPoint;
        }

        private ConnectionServerInfo connectionServer = new ConnectionServerInfo();

        private NetworkConnectionManager()
        {
        }

        public static NetworkConnectionManager GetNetworkConnectionManager()
        {
            if (networkConnectionManager == null)
            {
                networkConnectionManager = new NetworkConnectionManager();
            }
            return networkConnectionManager;
        }

        public Response Send(Request request)
        {
            Response response = null;
            try
            {
                this.SetParamsConnection();
                connectionServer.connectionToServerSocket.Connect(connectionServer.ipEndPoint);

                if (connectionServer.connectionToServerSocket.Connected)
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings
                    {
                        Formatting = Formatting.Indented,
                        TypeNameHandling = TypeNameHandling.Objects
                    };
                    settings.Converters.Add(new BigIntegerConverter());
                    string serializeString = JsonConvert.SerializeObject(request,
                                settings);
                    connectionServer.connectionToServerSocket.Send(
                        Properties.Settings.Default.gzipCompress ?
                        Compress(Encoding.GetEncoding(1251).GetBytes(serializeString))
                        : Encoding.GetEncoding(1251).GetBytes(serializeString));

                    if (request.waitResponse)
                    {
                        //Макисмум 50 мБайт
                        byte[] buffer = new byte[52428800];
                        int sizeReceive = connectionServer.connectionToServerSocket.Receive(buffer);
                        if (sizeReceive > 0)
                        {
                            byte[] res = new byte[sizeReceive];
                            Array.Copy(buffer, res, sizeReceive);

                            string serialzString = Encoding.GetEncoding(1251).GetString(
                                Properties.Settings.Default.gzipCompress ?
                                Decompress(res)
                                : res);
                            JsonSerializerSettings settings1 = new JsonSerializerSettings
                            {
                                Formatting = Formatting.Indented,
                                TypeNameHandling = TypeNameHandling.Objects,
                            };
                            settings1.Converters.Add(new BigIntegerConverter());
                            response = JsonConvert.DeserializeObject<Response>(serialzString,
                                settings1);

                            if (response is ResponseGetOpenContent responseOpenBook)
                            {
                                byte[] bufferForContent = new byte[responseOpenBook.sendedSizeContent];
                                sizeReceive = 0;
                                while(sizeReceive < responseOpenBook.sendedSizeContent)
                                {
                                    sizeReceive += connectionServer.connectionToServerSocket.Receive(bufferForContent, sizeReceive, responseOpenBook.sendedSizeContent - sizeReceive, SocketFlags.None);
                                }
                                if (sizeReceive > 0)
                                {
                                    responseOpenBook.content.ContentData =
                                        Properties.Settings.Default.gzipCompress ? Decompress(bufferForContent) : bufferForContent;
                                }
                            }
                        }
                    }
                    connectionServer.connectionToServerSocket.Close();
                }
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
            return response;
        }

        private void SetParamsConnection()
        {
            try
            {
                IPAddress iPAddress = IPAddress.Parse(Properties.Settings.Default.ipAddressServer);
                this.connectionServer.ipEndPoint = new IPEndPoint(
                    iPAddress,
                    Properties.Settings.Default.port);
                this.connectionServer.connectionToServerSocket = new Socket(
                    iPAddress.AddressFamily,
                    SocketType.Stream,
                    ProtocolType.Tcp);
                this.connectionServer.connectionToServerSocket.ReceiveTimeout = Properties.Settings.Default.reciveTimeOut;
                this.connectionServer.connectionToServerSocket.SendTimeout = Properties.Settings.Default.sendTimeOut;
            }
            catch (Exception exept)
            {
                logger.Error(exept);
            }
        }

        public byte[] Compress(byte[] data)
        {
            using (var compressedStream = new MemoryStream())
            {
                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
                {
                    zipStream.Write(data, 0, data.Length);
                    zipStream.Close();
                    return compressedStream.ToArray();
                }
            }
        }

        public byte[] Decompress(byte[] data)
        {
            using (var compressedStream = new MemoryStream(data))
            {
                using (var zipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
                {
                    using (var resultStream = new MemoryStream())
                    {
                        zipStream.CopyTo(resultStream);
                        return resultStream.ToArray();
                    }
                }
            }
        }
    }
}
