﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.Controller;
using ImageMagick;

namespace Client.Forms
{
    public partial class MainWindow : Form
    {
        private readonly WindowController windowController;
        private MagickImageCollection contentNowOpen;
        private int nowPage = 1;
        private int countPage;

        private int scale = 0;

        public MainWindow(WindowController windowController)
        {
            InitializeComponent();
            this.windowController = windowController;
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            windowController.Logout(labelLogin.Text);
        }

        public void SetLoginLabel(string Login)
        {
            labelLogin.Text = Login;
        }

        public void buttonRefreshTreeContents_Click(object sender, EventArgs e)
        {
            windowController.RefreshContentsTree(this.treeViewContents);
        }

        private void buttonBuyContent_Click(object sender, EventArgs e)
        {
            TreeNode selectedContent = treeViewContents.SelectedNode;
            this.windowController.BuyContent(Convert.ToInt32(selectedContent.Name));
            this.buttonRefreshTreeContents_Click(null, null);
        }

        private void buttonDeleteContent_Click(object sender, EventArgs e)
        {
            TreeNode selectedContent = treeViewContents.SelectedNode;
            this.windowController.DeleteContent(Convert.ToInt32(selectedContent.Name));
            this.buttonRefreshTreeContents_Click(null, null);
        }

        private async void buttonOpenContent_Click(object sender, EventArgs e)
        {
            TreeNode selectedContent = treeViewContents.SelectedNode;
            this.contentNowOpen = this.windowController.OpenContent(Convert.ToInt32(selectedContent.Name), out bool needUpdate);
            //this.dataContent = this.windowController.OpenContent(Convert.ToInt32(selectedContent.Name), labelLogin.Text);

            if (this.contentNowOpen != null)
            {
                this.nowPage = 1;
                this.countPage = this.contentNowOpen.Count;

                this.scale = -20;
                checkEnableButtonsZoom();

                showPage(this.contentNowOpen[nowPage - 1].ToBitmap());

                this.buttonCloseContent.Show();
                this.buttonPrevPage.Show();
                this.buttonPrevPage.Enabled = false;
                this.buttonNextPage.Show();
                this.buttonNextPage.Enabled = true;

                this.buttonZoomPlus.Show();
                this.buttonZoomMinus.Show();
            }

            if (needUpdate)
            {
                await Task.Run(new Action(() => this.windowController.UpdateSessionKey()));
            }
        }

        private void treeViewContents_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Parent != null && e.Node.Parent.Name.Equals("allContents"))
            {
                if (treeViewContents.Nodes[treeViewContents.Nodes.IndexOfKey("myContents")].Nodes.ContainsKey(e.Node.Name))
                {
                    this.buttonBuyContent.Enabled = false;
                    this.buttonDeleteContent.Enabled = true;
                    this.buttonOpenContent.Enabled = true;
                }
                else
                {
                    this.buttonBuyContent.Enabled = true;
                    this.buttonDeleteContent.Enabled = false;
                    this.buttonOpenContent.Enabled = false;
                }
            }
            else
            {
                if(e.Node.Parent != null && e.Node.Parent.Name.Equals("myContents"))
                {
                    this.buttonDeleteContent.Enabled = true;
                    this.buttonOpenContent.Enabled = true;
                }
                this.buttonBuyContent.Enabled = false;
            }
        }

        private void buttonNextPage_Click(object sender, EventArgs e)
        {
            this.nowPage = this.nowPage + 1 >= this.countPage ? this.countPage : this.nowPage + 1;
            showPage(this.contentNowOpen[this.nowPage-1].ToBitmap());
            checkEnableButtonsNextPrev();
        }

        private void buttonPrevPage_Click(object sender, EventArgs e)
        {
            this.nowPage = this.nowPage -1 <= 1 ? 1 : this.nowPage - 1;
            showPage(this.contentNowOpen[this.nowPage-1].ToBitmap());
            checkEnableButtonsNextPrev();
        }

        private void checkEnableButtonsNextPrev()
        {
            if (this.nowPage == this.countPage)
            {
                this.buttonNextPage.Enabled = false;
                this.buttonPrevPage.Enabled = true;
                return;
            }

            if (this.nowPage == 1)
            {
                this.buttonNextPage.Enabled = true;
                this.buttonPrevPage.Enabled = false;
                return;
            }

            this.buttonNextPage.Enabled = true;
            this.buttonPrevPage.Enabled = true;
        }

        private void checkEnableButtonsZoom()
        {
            if (this.scale == 0)
            {
                this.buttonZoomPlus.Enabled = false;
                this.buttonZoomMinus.Enabled = true;
                return;
            }
            if (this.scale == -60)
            {
                this.buttonZoomPlus.Enabled = true;
                this.buttonZoomMinus.Enabled = false;
                return;
            }

            this.buttonZoomPlus.Enabled = true;
            this.buttonZoomMinus.Enabled = true;
        }

        private void panelViewContent_Resize(object sender, EventArgs e)
        {
            if (panelViewContent.Width >= pictureBoxShowPage.Size.Width)
            {
                pictureBoxShowPage.Anchor = AnchorStyles.Top;
            }
            else
            {
                if (panelViewContent.Width < pictureBoxShowPage.Size.Width)
                {
                    pictureBoxShowPage.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }
            }
        }

        private void buttonCloseContent_Click(object sender, EventArgs e)
        {
            this.pictureBoxShowPage.Image = null;
            this.pictureBoxShowPage.Size = new System.Drawing.Size(100, 100);
            this.buttonCloseContent.Hide();
            this.buttonNextPage.Hide();
            this.buttonPrevPage.Hide();

            this.buttonZoomPlus.Hide();
            this.buttonZoomMinus.Hide();

            this.nowPage = 1;
            this.countPage = 0;
            this.contentNowOpen.Dispose();
            this.contentNowOpen = null;
        }

        private void buttonZoomPlus_Click(object sender, EventArgs e)
        {
            this.scale = this.scale + 10 >= 0 ? 0 : this.scale + 10;
            showPage(this.contentNowOpen[nowPage - 1].ToBitmap());
            checkEnableButtonsZoom();
        }

        private void buttonZoomMinus_Click(object sender, EventArgs e)
        {
            this.scale = this.scale - 10 <= -60 ? -60 : this.scale - 10;
            showPage(this.contentNowOpen[nowPage - 1].ToBitmap());
            checkEnableButtonsZoom();
        }

        private Image Zoom(Image img)
        {
            /*MagickReadSettings magickSetting = new MagickReadSettings();
            magickSetting.Compression = CompressionMethod.JPEG2000;
            magickSetting.FrameIndex = nowPage - 1;
            magickSetting.Density = new Density(175, 175);

            Image nowPageImage = new MagickImageCollection(dataContent, magickSetting).ToBitmap();
            
            int width = nowPageImage.Width + (nowPageImage.Width * this.scale / 100);
            int height = nowPageImage.Height + (nowPageImage.Height * this.scale / 100);
             */

            int width = contentNowOpen[nowPage - 1].Width + (contentNowOpen[nowPage - 1].Width * this.scale / 100);
            int height = contentNowOpen[nowPage - 1].Height + (contentNowOpen[nowPage - 1].Height * this.scale / 100);

            Bitmap bmp = new Bitmap(img, width, height);
            Graphics g = Graphics.FromImage(bmp);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            return bmp;
        }

        private void showPage(Image image)
        {
            this.pictureBoxShowPage.Image = Zoom(image);
        }

        private void panelViewContent_SizeChanged(object sender, EventArgs e)
        {
            pictureBoxShowPage.Location = new Point(0, 0);
            if (pictureBoxShowPage.Width < panelViewContent.Width)
            {
                pictureBoxShowPage.Location = new Point((panelViewContent.Width - pictureBoxShowPage.Width)/2, 0);
            }
        }
    }
}
