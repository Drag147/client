﻿namespace Client.Forms
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Весь контент");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Мой контент");
            this.labelLogin = new System.Windows.Forms.Label();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.treeViewContents = new System.Windows.Forms.TreeView();
            this.buttonRefreshTreeContents = new System.Windows.Forms.Button();
            this.buttonBuyContent = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonDeleteContent = new System.Windows.Forms.Button();
            this.buttonOpenContent = new System.Windows.Forms.Button();
            this.panelViewContent = new System.Windows.Forms.Panel();
            this.pictureBoxShowPage = new System.Windows.Forms.PictureBox();
            this.buttonNextPage = new System.Windows.Forms.Button();
            this.buttonPrevPage = new System.Windows.Forms.Button();
            this.buttonCloseContent = new System.Windows.Forms.Button();
            this.buttonZoomMinus = new System.Windows.Forms.Button();
            this.buttonZoomPlus = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panelViewContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPage)).BeginInit();
            this.SuspendLayout();
            // 
            // labelLogin
            // 
            this.labelLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLogin.Location = new System.Drawing.Point(788, 14);
            this.labelLogin.MaximumSize = new System.Drawing.Size(97, 19);
            this.labelLogin.MinimumSize = new System.Drawing.Size(97, 19);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(97, 19);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonLogout
            // 
            this.buttonLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLogout.AutoSize = true;
            this.buttonLogout.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLogout.Location = new System.Drawing.Point(891, 12);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(57, 25);
            this.buttonLogout.TabIndex = 1;
            this.buttonLogout.Text = "Выйти";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // treeViewContents
            // 
            this.treeViewContents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeViewContents.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeViewContents.Location = new System.Drawing.Point(6, 25);
            this.treeViewContents.Name = "treeViewContents";
            treeNode27.Name = "allContents";
            treeNode27.Text = "Весь контент";
            treeNode28.Name = "myContents";
            treeNode28.Text = "Мой контент";
            this.treeViewContents.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode27,
            treeNode28});
            this.treeViewContents.Size = new System.Drawing.Size(268, 284);
            this.treeViewContents.TabIndex = 2;
            this.treeViewContents.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewContents_NodeMouseClick);
            // 
            // buttonRefreshTreeContents
            // 
            this.buttonRefreshTreeContents.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRefreshTreeContents.AutoSize = true;
            this.buttonRefreshTreeContents.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRefreshTreeContents.Location = new System.Drawing.Point(6, 315);
            this.buttonRefreshTreeContents.Name = "buttonRefreshTreeContents";
            this.buttonRefreshTreeContents.Size = new System.Drawing.Size(71, 25);
            this.buttonRefreshTreeContents.TabIndex = 3;
            this.buttonRefreshTreeContents.Text = "Обновить";
            this.buttonRefreshTreeContents.UseVisualStyleBackColor = true;
            this.buttonRefreshTreeContents.Click += new System.EventHandler(this.buttonRefreshTreeContents_Click);
            // 
            // buttonBuyContent
            // 
            this.buttonBuyContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBuyContent.AutoSize = true;
            this.buttonBuyContent.Enabled = false;
            this.buttonBuyContent.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonBuyContent.Location = new System.Drawing.Point(6, 345);
            this.buttonBuyContent.Name = "buttonBuyContent";
            this.buttonBuyContent.Size = new System.Drawing.Size(191, 25);
            this.buttonBuyContent.TabIndex = 4;
            this.buttonBuyContent.Text = "Получить выбранный контент";
            this.buttonBuyContent.UseVisualStyleBackColor = true;
            this.buttonBuyContent.Click += new System.EventHandler(this.buttonBuyContent_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.buttonDeleteContent);
            this.groupBox1.Controls.Add(this.buttonOpenContent);
            this.groupBox1.Controls.Add(this.buttonBuyContent);
            this.groupBox1.Controls.Add(this.treeViewContents);
            this.groupBox1.Controls.Add(this.buttonRefreshTreeContents);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 376);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Контент";
            // 
            // buttonDeleteContent
            // 
            this.buttonDeleteContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDeleteContent.AutoSize = true;
            this.buttonDeleteContent.Enabled = false;
            this.buttonDeleteContent.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDeleteContent.Location = new System.Drawing.Point(203, 345);
            this.buttonDeleteContent.Name = "buttonDeleteContent";
            this.buttonDeleteContent.Size = new System.Drawing.Size(71, 25);
            this.buttonDeleteContent.TabIndex = 6;
            this.buttonDeleteContent.Text = "Удалить";
            this.buttonDeleteContent.UseVisualStyleBackColor = true;
            this.buttonDeleteContent.Click += new System.EventHandler(this.buttonDeleteContent_Click);
            // 
            // buttonOpenContent
            // 
            this.buttonOpenContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOpenContent.AutoSize = true;
            this.buttonOpenContent.Enabled = false;
            this.buttonOpenContent.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOpenContent.Location = new System.Drawing.Point(83, 315);
            this.buttonOpenContent.Name = "buttonOpenContent";
            this.buttonOpenContent.Size = new System.Drawing.Size(191, 25);
            this.buttonOpenContent.TabIndex = 5;
            this.buttonOpenContent.Text = "Открыть выбранный контент";
            this.buttonOpenContent.UseVisualStyleBackColor = true;
            this.buttonOpenContent.Click += new System.EventHandler(this.buttonOpenContent_Click);
            // 
            // panelViewContent
            // 
            this.panelViewContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelViewContent.AutoScroll = true;
            this.panelViewContent.BackColor = System.Drawing.SystemColors.Control;
            this.panelViewContent.Controls.Add(this.pictureBoxShowPage);
            this.panelViewContent.Location = new System.Drawing.Point(298, 12);
            this.panelViewContent.Name = "panelViewContent";
            this.panelViewContent.Size = new System.Drawing.Size(587, 376);
            this.panelViewContent.TabIndex = 5;
            this.panelViewContent.SizeChanged += new System.EventHandler(this.panelViewContent_SizeChanged);
            // 
            // pictureBoxShowPage
            // 
            this.pictureBoxShowPage.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxShowPage.Name = "pictureBoxShowPage";
            this.pictureBoxShowPage.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxShowPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxShowPage.TabIndex = 0;
            this.pictureBoxShowPage.TabStop = false;
            this.pictureBoxShowPage.SizeChanged += new System.EventHandler(this.panelViewContent_SizeChanged);
            // 
            // buttonNextPage
            // 
            this.buttonNextPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextPage.AutoSize = true;
            this.buttonNextPage.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonNextPage.Location = new System.Drawing.Point(891, 332);
            this.buttonNextPage.Name = "buttonNextPage";
            this.buttonNextPage.Size = new System.Drawing.Size(57, 25);
            this.buttonNextPage.TabIndex = 5;
            this.buttonNextPage.Text = "→";
            this.buttonNextPage.UseVisualStyleBackColor = true;
            this.buttonNextPage.Visible = false;
            this.buttonNextPage.Click += new System.EventHandler(this.buttonNextPage_Click);
            // 
            // buttonPrevPage
            // 
            this.buttonPrevPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrevPage.AutoSize = true;
            this.buttonPrevPage.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPrevPage.Location = new System.Drawing.Point(891, 363);
            this.buttonPrevPage.Name = "buttonPrevPage";
            this.buttonPrevPage.Size = new System.Drawing.Size(57, 25);
            this.buttonPrevPage.TabIndex = 6;
            this.buttonPrevPage.Text = "←";
            this.buttonPrevPage.UseVisualStyleBackColor = true;
            this.buttonPrevPage.Visible = false;
            this.buttonPrevPage.Click += new System.EventHandler(this.buttonPrevPage_Click);
            // 
            // buttonCloseContent
            // 
            this.buttonCloseContent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCloseContent.AutoSize = true;
            this.buttonCloseContent.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCloseContent.Location = new System.Drawing.Point(891, 43);
            this.buttonCloseContent.Name = "buttonCloseContent";
            this.buttonCloseContent.Size = new System.Drawing.Size(63, 25);
            this.buttonCloseContent.TabIndex = 7;
            this.buttonCloseContent.Text = "Закрыть";
            this.buttonCloseContent.UseVisualStyleBackColor = true;
            this.buttonCloseContent.Visible = false;
            this.buttonCloseContent.Click += new System.EventHandler(this.buttonCloseContent_Click);
            // 
            // buttonZoomMinus
            // 
            this.buttonZoomMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonZoomMinus.AutoSize = true;
            this.buttonZoomMinus.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonZoomMinus.Location = new System.Drawing.Point(891, 301);
            this.buttonZoomMinus.Name = "buttonZoomMinus";
            this.buttonZoomMinus.Size = new System.Drawing.Size(57, 25);
            this.buttonZoomMinus.TabIndex = 9;
            this.buttonZoomMinus.Text = "-";
            this.buttonZoomMinus.UseVisualStyleBackColor = true;
            this.buttonZoomMinus.Visible = false;
            this.buttonZoomMinus.Click += new System.EventHandler(this.buttonZoomMinus_Click);
            // 
            // buttonZoomPlus
            // 
            this.buttonZoomPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonZoomPlus.AutoSize = true;
            this.buttonZoomPlus.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonZoomPlus.Location = new System.Drawing.Point(891, 270);
            this.buttonZoomPlus.Name = "buttonZoomPlus";
            this.buttonZoomPlus.Size = new System.Drawing.Size(57, 25);
            this.buttonZoomPlus.TabIndex = 8;
            this.buttonZoomPlus.Text = "+";
            this.buttonZoomPlus.UseVisualStyleBackColor = true;
            this.buttonZoomPlus.Visible = false;
            this.buttonZoomPlus.Click += new System.EventHandler(this.buttonZoomPlus_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 400);
            this.Controls.Add(this.buttonZoomMinus);
            this.Controls.Add(this.buttonZoomPlus);
            this.Controls.Add(this.buttonCloseContent);
            this.Controls.Add(this.buttonPrevPage);
            this.Controls.Add(this.buttonNextPage);
            this.Controls.Add(this.panelViewContent);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.labelLogin);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainWindow";
            this.Text = "Главное окно";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelViewContent.ResumeLayout(false);
            this.panelViewContent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShowPage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.TreeView treeViewContents;
        private System.Windows.Forms.Button buttonRefreshTreeContents;
        private System.Windows.Forms.Button buttonBuyContent;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panelViewContent;
        private System.Windows.Forms.PictureBox pictureBoxShowPage;
        private System.Windows.Forms.Button buttonNextPage;
        private System.Windows.Forms.Button buttonPrevPage;
        private System.Windows.Forms.Button buttonOpenContent;
        private System.Windows.Forms.Button buttonDeleteContent;
        private System.Windows.Forms.Button buttonCloseContent;
        private System.Windows.Forms.Button buttonZoomMinus;
        private System.Windows.Forms.Button buttonZoomPlus;
    }
}

