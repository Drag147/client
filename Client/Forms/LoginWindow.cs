﻿using System;
using System.Windows.Forms;
using Client.Controller;
using DrmCommon.Request;

namespace Client.Forms
{
    public partial class LoginWindow : Form
    {
        private readonly WindowController windowController;

        public LoginWindow(WindowController windowController)
        {
            InitializeComponent();
            this.windowController = windowController;
        }

        private void buttonLoging_Click(object sender, EventArgs e)
        {
            this.windowController.Login(TypeRequest.RequestAuthorization, textBoxLogin.Text, textBoxPassword.Text);
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            this.windowController.ShowRegistrationForm();
        }

        private void textBoxLogin_TextChanged(object sender, EventArgs e)
        {
            CheckActivateButtonLogin();
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            CheckActivateButtonLogin();
        }

        public void SetTextBoxes (string Login, string Password)
        {
            textBoxLogin.Text = Login;
            textBoxPassword.Text = Password;
        }

        private void CheckActivateButtonLogin()
        {
            if (textBoxLogin.Text.Length >= 5 && textBoxPassword.Text.Length >= 5)
            {
                buttonLoging.Enabled = true;
                return;
            }
            buttonLoging.Enabled = false;
        }

        public void ClearForm()
        {
            textBoxLogin.Clear();
            textBoxPassword.Clear();
        }
    }
}
