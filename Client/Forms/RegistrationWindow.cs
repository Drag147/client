﻿using System.Drawing;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.Controller;
using DrmCommon.Request;

namespace Client.Forms
{
    public partial class RegistrationWindow : Form
    {
        private readonly WindowController windowController;

        public RegistrationWindow(WindowController windowController)
        {
            InitializeComponent();
            this.windowController = windowController;
        }

        private void buttonRegistration_Click(object sender, System.EventArgs e)
        {
            this.windowController.Registration(
                TypeRequest.Registration,
                textBoxLogin.Text,
                textBoxPassword.Text,
                textBoxName.Text,
                textBoxSurname.Text,
                textBoxPatronymic.Text,
                textBoxMail.Text);
        }

        private void textBoxLogin_TextChanged(object sender, System.EventArgs e)
        {
            CheckEnableButton();
        }

        private void textBoxPassword_TextChanged(object sender, System.EventArgs e)
        {
            CheckEnableButton();
        }

        private void textBoxRepeatPassword_TextChanged(object sender, System.EventArgs e)
        {
            CheckEnableButton();
        }

        public void ClearLoginAndPassword()
        {
            textBoxLogin.Clear();
            textBoxPassword.Clear();
            textBoxRepeatPassword.Clear();
            textBoxLogin.Focus();
        }

        public void CheckEnableButton()
        {
            if (textBoxLogin.Text.Length >=5 && textBoxPassword.Text.Length >= 5 
                && textBoxRepeatPassword.Text.Length == textBoxPassword.Text.Length &&
                textBoxPassword.Text.Equals(textBoxPassword.Text))
            {
                buttonRegistration.Enabled = true;
                return;
            }
            buttonRegistration.Enabled = false;
        }

        private void textBoxFocus_Leave(object sender, System.EventArgs e)
        {
            if (sender is TextBox textBox)
            {
                if (textBox.Text.Length < 5)
                {
                    NotifyErrorField(textBox, 2);
                    toolTipMinLenght.Show("Минимальная длина поля 5 символов", textBox, 1500);
                }
                else
                {
                    if (textBox != textBoxLogin)
                    {
                        if (textBoxPassword.Text != textBoxRepeatPassword.Text)
                        {
                            NotifyErrorField(textBoxPassword, 2);
                            NotifyErrorField(textBoxRepeatPassword, 2);
                            toolTipMinLenght.Show("Значение полей должны совпадать", textBox, 1500);
                        }
                        else
                        {
                            NotifyErrorField(textBoxPassword, 1);
                            NotifyErrorField(textBoxRepeatPassword, 1);
                        }
                    }
                    else
                    {
                        NotifyErrorField(textBox, 1);
                    }
                }
            }
        }

        private void textBoxMail_Leave(object sender, System.EventArgs e)
        {
            if (checkEmailForMask(textBoxMail.Text))
            {
                NotifyErrorField(textBoxMail, 1);
            }
            else
            {
                textBoxMail.Clear();
                toolTipMinLenght.Show("Поле должно соотвествовать маске для e-mail", textBoxMail, 1500);
                NotifyErrorField(textBoxMail, 2);
            }
        }

        private bool checkEmailForMask(string email)
        {
            string theEmailPattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                   + "@"
                                   + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";

            return Regex.IsMatch(email, theEmailPattern);
        }

        //state = 0 - Default, 1 - Green, 2 - Red
        async void NotifyErrorField(TextBox textBox, int state)
        {
            switch (state)
            {
                case 0:
                    textBox.BackColor = Color.White;
                    break;
                case 1:
                    textBox.BackColor = Color.LawnGreen;
                    await Task.Delay(1000);
                    NotifyErrorField(textBox, 0);
                    break;
                case 2:
                    textBox.BackColor = Color.Red;
                    await Task.Delay(1000);
                    NotifyErrorField(textBox, 0);
                    break;
                default:
                    textBox.BackColor = Color.White;
                    break;
            }
        }
    }
}
